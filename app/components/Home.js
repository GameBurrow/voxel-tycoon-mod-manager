// @flow
import React, { Component, useState } from 'react';
import fixNewLinesInJsonStrings from 'fix-newlines-in-json-strings';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';
import styles from './Home.css';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  constructor(props) {
    super(props);
    this.state = {
      modList: {}
    };
  }

  componentDidMount() {
    this.setState(
      {
        // eslint-disable-next-line global-require
        modList: require(`D:/Itch games/voxel-tycoon/Content/packs.json`)
      },
      () => {
        if (this.state.modList.length > 0) {
          this.state.modList.map((item, index) => {
            this.tryToParseModJSON(index);
          });
        }
      }
    );
  }

  // Needed because Voxel Tycoon Mods can have newline strings
  tryToParseModJSON(key) {
    const path = `D:/Itch games/voxel-tycoon/Content/${
      this.state.modList[key].Name
    }/pack.json`;
    // eslint-disable-next-line global-require
    const fs = require('fs');

    fs.readFile(path, 'utf-8', (err, data) => {
      if (err) return;

      // Converting Raw Buffer to text
      // data using tostring function.
      const fixedJSON = fixNewLinesInJsonStrings(data);
      const jsonData = JSON.parse(fixedJSON);

      try {
        // eslint-disable-next-line global-require,import/no-dynamic-require
        jsonData.Image = require(`D:/Itch games/voxel-tycoon/Content/${
          this.state.modList[key].Name
        }/preview.png`);
      } catch (errImage) {
        console.log(errImage);
        jsonData.Image = false;
      }

      this.setState({
        modList: Object.values({
          ...this.state.modList,
          [key]: {
            ...this.state.modList[key],
            ...jsonData
          }
        })
      });
    });
  }

  render() {
    return (
      <div className={styles.container} data-tid="container">
        <Typography variant="h2" gutterBottom>
          List of mods
        </Typography>
        <Grid container spacing={3} justify="center" alignItems="stretch">
          {this.state.modList.length &&
            this.state.modList.map(item => {
              return (
                <Grid Item>
                  <Card key={item.name} className="modItem">
                    <CardHeader
                      title={item.Title ? item.Title : item.Name}
                      subheader={`Updated on ${new Date(
                        item.Updated
                      ).toLocaleString()}`}
                    />
                    {item.Image && (
                      <CardMedia
                        image={`D:/Itch games/voxel-tycoon/Content/${
                          item.Name
                        }/preview.png`}
                        title={item.Title ? item.Title : item.Name}
                        component="img"
                      />
                    )}
                    <CardContent>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        component="p"
                      >
                        {item.Description}
                      </Typography>
                    </CardContent>
                    <CardActions>
                      {!item.Disabled && (
                        <Button size="small" color="error">
                          Disable
                        </Button>
                      )}
                      {item.Disabled && (
                        <Button size="small" color="success">
                          Enable
                        </Button>
                      )}
                    </CardActions>
                  </Card>
                </Grid>
              );
            })}
        </Grid>
      </div>
    );
  }
}
