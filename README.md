# Voxel Tycoon Mod Manager

## Using
Electron

[Electron React Boilerplate](https://electron-react-boilerplate.js.org) - Git 2019-07-31

### Extra added Packages
json-loader

## Install

Iinstall the dependencies with yarn.

```bash
$ yarn
```

## Starting Development

Start the app in the `dev` environment. This starts the renderer process in [**hot-module-replacement**](https://webpack.js.org/guides/hmr-react/) mode and starts a webpack dev server that sends hot updates to the renderer process:

```bash
$ yarn dev
```
